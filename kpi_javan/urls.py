from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from magang_kpi_javan import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^register/$', views.CreateUser, name="registers"),
    url(r'^AdminPageUser/$', views.AdminPageUser, name="adminuserpage"),
    url(r'^AdminPageDivisi/$', views.AdminPageDivisi, name="admindivisipage"),
    url(r'^login/$', views.LoginUser, name="login"),
    url(r'^loginAdmin/$', views.LoginAdmin, name="loginAdmin"),
    url(r'^indexManager/$', views.IndexManager, name="indexManager"),
    url(r'^indexManager/(?P<jenisKpi>[^/]+)/(?P<id>[0-9]+)/$', views.KpiManager, name="kpimanager"),
    url(r'^indexKaryawan/(?P<jenisKpi>[^/]+)/$', views.IndexKaryawan, name="indexKaryawan"),
    url(r'^indexKaryawan/(?P<jenisKpi>[^/]+)/(?P<selected_day>\d{4}-\d{2}-\d{2})/$', views.IndexKaryawanDate, name="indexKaryawanDate"),
]
#?P<jenisKpi>[-\w\d]+
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)