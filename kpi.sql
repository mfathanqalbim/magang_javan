-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 05, 2021 at 12:23 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kpi`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add user', 4, 'add_user'),
(14, 'Can change user', 4, 'change_user'),
(15, 'Can delete user', 4, 'delete_user'),
(16, 'Can view user', 4, 'view_user'),
(17, 'Can add content type', 5, 'add_contenttype'),
(18, 'Can change content type', 5, 'change_contenttype'),
(19, 'Can delete content type', 5, 'delete_contenttype'),
(20, 'Can view content type', 5, 'view_contenttype'),
(21, 'Can add session', 6, 'add_session'),
(22, 'Can change session', 6, 'change_session'),
(23, 'Can delete session', 6, 'delete_session'),
(24, 'Can view session', 6, 'view_session'),
(25, 'Can add kpi', 9, 'add_kpi'),
(26, 'Can change kpi', 9, 'change_kpi'),
(27, 'Can delete kpi', 9, 'delete_kpi'),
(28, 'Can view kpi', 9, 'view_kpi'),
(29, 'Can add review', 10, 'add_review'),
(30, 'Can change review', 10, 'change_review'),
(31, 'Can delete review', 10, 'delete_review'),
(32, 'Can view review', 10, 'view_review'),
(33, 'Can add role', 7, 'add_role'),
(34, 'Can change role', 7, 'change_role'),
(35, 'Can delete role', 7, 'delete_role'),
(36, 'Can view role', 7, 'view_role'),
(37, 'Can add users', 8, 'add_users'),
(38, 'Can change users', 8, 'change_users'),
(39, 'Can delete users', 8, 'delete_users'),
(40, 'Can view users', 8, 'view_users'),
(41, 'Can add user', 11, 'add_user'),
(42, 'Can change user', 11, 'change_user'),
(43, 'Can delete user', 11, 'delete_user'),
(44, 'Can view user', 11, 'view_user'),
(45, 'Can add divisi', 12, 'add_divisi'),
(46, 'Can change divisi', 12, 'change_divisi'),
(47, 'Can delete divisi', 12, 'delete_divisi'),
(48, 'Can view divisi', 12, 'view_divisi');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$216000$gO7t1TljzWak$ByH4/iecagDWHBwo2swDPmw2agagpVzKZ0XWfPPakPI=', '2021-05-04 09:21:30.922653', 1, 'admin', '', '', 'admin@mail.com', 1, 1, '2021-04-21 08:51:32.887427'),
(27, 'pbkdf2_sha256$216000$WBcxy6Cu26ok$uWe3z/itpuovHDZvOyPMreGoiosI+iLiM56bjNN8xjQ=', '2021-05-04 14:29:48.891108', 0, 'test21', '', '', 'test2@gmail.com', 0, 1, '2021-04-30 03:26:41.000000'),
(28, 'pbkdf2_sha256$216000$Is5M1mxdpOuw$yESoWYqinjEn3di9vbnEf3GHave4HJNBEIldkdz1Blk=', '2021-05-03 02:44:55.025482', 0, 'test3', '', '', 'test3@gmail.com', 0, 1, '2021-04-30 03:32:41.178737'),
(29, 'pbkdf2_sha256$216000$NUOVtaT81HFl$OVZ+PU4CNf5/Mb9mqKXXgV3V1chM01UbeD3NV5j0dgg=', '2021-05-03 05:31:09.596299', 0, 'test4', '', '', 'test4@gmail.com', 0, 1, '2021-04-30 03:33:05.874005'),
(30, 'pbkdf2_sha256$216000$NOEaq7BKl5vW$avU4tNKfOptfgz9nvoDzoNfPAVsY/Sozw/ruA3i/QuA=', NULL, 0, 'test5', '', '', 'test5@gmail.com', 0, 1, '2021-04-30 03:33:31.901627'),
(31, 'pbkdf2_sha256$216000$9I01TcZmNTS1$NEnwczympLyBC9YPKjMKLReYd74+T1kz+/DRI5DyzzM=', '2021-05-03 05:30:30.085802', 0, 'test6', '', '', 'test6@gmail.com', 0, 1, '2021-04-30 03:33:53.780422'),
(32, 'pbkdf2_sha256$216000$4tDMVaM8C1iU$3a+tHyqSgaIPFaOABu+aQ47gEtz0BrWR/jdzmDhFVd8=', '2021-05-04 08:05:47.254091', 0, 'tester', '', '', 'tester@gmail.com', 0, 1, '2021-05-03 02:08:10.266035'),
(33, 'pbkdf2_sha256$216000$aRewJRdKVmHn$PrGB05aIYOl6lpUuVOpRRpTlXqu8BhcrlddgShOd2co=', '2021-05-04 14:32:56.364521', 0, 'test13', '', '', 'test1@gmail.com', 0, 1, '2021-05-03 02:50:01.000000'),
(34, 'pbkdf2_sha256$216000$XNK2PlqdB5Os$iCurEDVhqNVC3AHjYKNZ0zSa5A25xtIrk+QaDSe0iM0=', '2021-05-03 05:23:49.728369', 0, 'tester65', '', '', 'tester65@gmail.com', 0, 1, '2021-05-03 05:23:42.821910');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `divisi`
--

CREATE TABLE `divisi` (
  `id` int(10) NOT NULL,
  `divisi` varchar(30) NOT NULL,
  `posisi` varchar(30) NOT NULL,
  `jenis_kpi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `divisi`
--

INSERT INTO `divisi` (`id`, `divisi`, `posisi`, `jenis_kpi`) VALUES
(59447533, 'TEST 1', 'TEST 1', 'TEST1, TEST2, TEST3'),
(62194996, 'Project Manager', 'Project Manager', 'Planned Hours Vs. Time Spent,Rework due to gap in requirement,Number Of Cancelled Projects,Number of Pairing,Number of completed task each day'),
(81700465, 'System Analyst', 'System Analyst', 'test'),
(88400110, 'Engineer', 'Software Engineer', 'Average Downtime, Number of Bugs, Number of Comments per Merge Request, Number of Pairing, Number of completed task each day');

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL CHECK (`action_flag` >= 0),
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2021-04-21 09:19:34.763176', '1', '1', 1, '[{\"added\": {}}]', 7, 1),
(2, '2021-04-21 09:19:42.976434', '2', '2', 1, '[{\"added\": {}}]', 7, 1),
(3, '2021-04-21 18:34:30.785949', '94356639', '94356639', 3, '', 11, 1),
(4, '2021-04-21 18:34:36.868827', '2', 'muhammad_fathan_qalbi_madenda', 3, '', 4, 1),
(5, '2021-04-21 18:55:40.857503', '69176585', '69176585', 3, '', 11, 1),
(6, '2021-04-21 18:55:47.049372', '3', 'mfathan0708', 3, '', 4, 1),
(7, '2021-04-26 05:33:01.123322', '6', 'fathan', 3, '', 4, 1),
(8, '2021-04-26 05:33:01.127305', '4', 'mfathan0708', 3, '', 4, 1),
(9, '2021-04-26 05:33:01.129298', '7', 'm_fathan0708', 3, '', 4, 1),
(10, '2021-04-26 05:33:01.132302', '11', 'test1', 3, '', 4, 1),
(11, '2021-04-26 05:33:01.135280', '9', 'test12', 3, '', 4, 1),
(12, '2021-04-26 05:33:01.136290', '8', 'test123', 3, '', 4, 1),
(13, '2021-04-26 05:33:01.138272', '12', 'test1234', 3, '', 4, 1),
(14, '2021-04-26 05:41:59.401606', '17', 'mfathan0708', 3, '', 4, 1),
(15, '2021-04-26 19:14:01.141542', '1', '1', 1, '[{\"added\": {}}]', 11, 1),
(16, '2021-04-26 19:14:27.213514', '1', '1', 3, '', 11, 1),
(17, '2021-04-27 17:28:17.002516', '19', 'mfathan0708', 3, '', 4, 1),
(18, '2021-04-27 17:28:17.006471', '20', 'm_fathan0708', 3, '', 4, 1),
(19, '2021-04-28 23:53:13.472695', '123', '123', 1, '[{\"added\": {}}]', 9, 1),
(20, '2021-04-28 23:54:12.402971', '44150222', '44150222', 1, '[{\"added\": {}}]', 10, 1),
(21, '2021-04-30 09:42:17.693726', '1', '1', 1, '[{\"added\": {}}]', 12, 1),
(22, '2021-04-30 09:42:44.139997', '2', '2', 1, '[{\"added\": {}}]', 12, 1),
(23, '2021-04-30 09:43:04.403857', '3', '3', 1, '[{\"added\": {}}]', 12, 1),
(24, '2021-04-30 09:43:27.095358', '3', '3', 3, '', 12, 1),
(25, '2021-04-30 09:43:27.098349', '2', '2', 3, '', 12, 1),
(26, '2021-04-30 09:43:27.101342', '1', '1', 3, '', 12, 1),
(27, '2021-05-02 06:00:24.980988', '1', '1', 1, '[{\"added\": {}}]', 12, 1),
(28, '2021-05-02 09:38:23.664366', '9855', '9855', 3, '', 9, 1),
(29, '2021-05-02 09:38:23.669499', '9398', '9398', 3, '', 9, 1),
(30, '2021-05-02 09:38:23.671328', '6300', '6300', 3, '', 9, 1),
(31, '2021-05-02 09:38:23.672316', '4744', '4744', 3, '', 9, 1),
(32, '2021-05-02 09:38:23.674309', '3804', '3804', 3, '', 9, 1),
(33, '2021-05-02 09:38:23.677302', '123', '123', 3, '', 9, 1),
(34, '2021-05-02 09:39:36.636792', '44150222', '44150222', 2, '[{\"changed\": {\"fields\": [\"Kpi\"]}}]', 10, 1),
(35, '2021-05-02 23:16:09.871758', '2008902', '2008902', 3, '', 10, 1),
(36, '2021-05-02 23:19:43.788781', '9022972', '9022972', 3, '', 10, 1),
(37, '2021-05-03 02:23:01.821030', '27', 'test21', 2, '[{\"changed\": {\"fields\": [\"Username\"]}}]', 4, 1),
(38, '2021-05-03 02:38:06.398135', '26', 'test12', 2, '[{\"changed\": {\"fields\": [\"Username\"]}}]', 4, 1),
(39, '2021-05-03 02:47:41.055926', '26', 'test1', 3, '', 4, 1),
(40, '2021-05-03 02:50:01.879139', '33', 'test1', 1, '[{\"added\": {}}]', 4, 1),
(41, '2021-05-03 02:57:11.799196', '33', 'test1', 2, '[{\"changed\": {\"fields\": [\"Username\"]}}]', 4, 1),
(42, '2021-05-03 02:57:49.282172', '33', 'test1', 2, '[{\"changed\": {\"fields\": [\"Username\"]}}]', 4, 1),
(43, '2021-05-03 03:08:34.301498', '23', 'mfathan0708', 3, '', 4, 1),
(44, '2021-05-03 03:08:34.305488', '24', 'm_fathan0708', 3, '', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(5, 'contenttypes', 'contenttype'),
(12, 'magang_kpi_javan', 'divisi'),
(9, 'magang_kpi_javan', 'kpi'),
(10, 'magang_kpi_javan', 'review'),
(7, 'magang_kpi_javan', 'role'),
(11, 'magang_kpi_javan', 'user'),
(8, 'magang_kpi_javan', 'users'),
(6, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2021-04-21 08:35:54.290892'),
(2, 'auth', '0001_initial', '2021-04-21 08:35:54.450097'),
(3, 'admin', '0001_initial', '2021-04-21 08:35:55.065879'),
(4, 'admin', '0002_logentry_remove_auto_add', '2021-04-21 08:35:55.200828'),
(5, 'admin', '0003_logentry_add_action_flag_choices', '2021-04-21 08:35:55.207810'),
(6, 'contenttypes', '0002_remove_content_type_name', '2021-04-21 08:35:55.273953'),
(7, 'auth', '0002_alter_permission_name_max_length', '2021-04-21 08:35:55.343380'),
(8, 'auth', '0003_alter_user_email_max_length', '2021-04-21 08:35:55.353355'),
(9, 'auth', '0004_alter_user_username_opts', '2021-04-21 08:35:55.359339'),
(10, 'auth', '0005_alter_user_last_login_null', '2021-04-21 08:35:55.421238'),
(11, 'auth', '0006_require_contenttypes_0002', '2021-04-21 08:35:55.423234'),
(12, 'auth', '0007_alter_validators_add_error_messages', '2021-04-21 08:35:55.429216'),
(13, 'auth', '0008_alter_user_username_max_length', '2021-04-21 08:35:55.439191'),
(14, 'auth', '0009_alter_user_last_name_max_length', '2021-04-21 08:35:55.465176'),
(15, 'auth', '0010_alter_group_name_max_length', '2021-04-21 08:35:55.481938'),
(16, 'auth', '0011_update_proxy_permissions', '2021-04-21 08:35:55.489419'),
(17, 'auth', '0012_alter_user_first_name_max_length', '2021-04-21 08:35:55.499393'),
(18, 'sessions', '0001_initial', '2021-04-21 08:35:55.532078'),
(19, 'magang_kpi_javan', '0001_initial', '2021-04-21 09:41:58.532479'),
(20, 'magang_kpi_javan', '0002_auto_20210422_0118', '2021-04-21 18:18:10.149837'),
(21, 'magang_kpi_javan', '0003_divisi', '2021-04-30 09:41:32.559529');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('cin7y6z149tf3l7xrsm8ef1517czgtl2', 'e30:1ldDht:haaVZuCZ-W4zTAxvTNGxcxYzAdp9oTGUk5T0uozk58U', '2021-05-16 15:08:29.751283'),
('de15jr6bbqau2rrhuzpudt6cgakr62gq', 'e30:1ldDgC:wvshv3riUpwc4jGWzNueOOT5dB3nF-HWTzw5SxMbEUI', '2021-05-16 15:06:44.994123'),
('f19jaaw3nydjunwz4iqv7jqouo86gqxd', 'e30:1ldDeV:6fL0woBz8sriKjYf43i3RYYlX9jObJvrSQi0MuD-6X8', '2021-05-16 15:04:59.115478'),
('g9gyc45gwogar4e0evfaeu8wnnfjdk0g', 'e30:1ldDfA:gh1sajfwqyVU9TxhkZx0_rfsj8CdpAoxqYSUBZuGYE0', '2021-05-16 15:05:40.989315'),
('ut45f3qmva36vw61w8fzzwy9a54t034u', 'e30:1lbSCT:BOdzI58oP-NBNW2bmON_J-yXhrnfveTMbmgzCzM7DB4', '2021-05-11 18:12:45.035832'),
('yvvvhyk17tnynxlidw1rolic9ioq9uci', 'e30:1ldDdl:hMKmqLNJovmy0B6qwAznjbQyu49D54eTo28xIAnFDH8', '2021-05-16 15:04:13.433640');

-- --------------------------------------------------------

--
-- Table structure for table `kpi`
--

CREATE TABLE `kpi` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `jenis_kpi` varchar(50) NOT NULL,
  `bobot` varchar(10) NOT NULL,
  `target` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kpi`
--

INSERT INTO `kpi` (`id`, `user_id`, `jenis_kpi`, `bobot`, `target`) VALUES
(437, 55997895, 'Planned Hours Vs. Time Spent', '10', '15'),
(6440, 35054603, 'Number of Bugs', '10', '15'),
(7472, 35054603, 'Number of Pairing', '10', '15'),
(7818, 35054603, 'Average Downtime', '10', '15'),
(9038, 35054603, 'Number of Comments per Merge Request', '10', '15'),
(9443, 35054603, 'Number of completed task each day', '10', '16');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `user_id` int(10) NOT NULL,
  `kpi_id` int(10) NOT NULL,
  `pencapaian` varchar(10) NOT NULL,
  `score` varchar(10) NOT NULL,
  `berkas` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `waktu` date NOT NULL,
  `note` text NOT NULL,
  `id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`user_id`, `kpi_id`, `pencapaian`, `score`, `berkas`, `status`, `waktu`, `note`, `id`) VALUES
(44150222, 7818, '15', '16', 'berkas_karyawan/Tutorial_Instal_Predator_Sense_di_Acer_Nitro_5_by_RiF-Tekno_qFBBwlR.pdf', 'Pending', '2021-05-05', '', 2255801),
(44150222, 9038, '13', '12', 'berkas_karyawan/Tutorial_Instal_Predator_Sense_di_Acer_Nitro_5_by_RiF-Tekno_VbvNdkl.pdf', 'Pending', '2021-05-04', '', 2298640),
(44150222, 9038, '14', '2', 'http://127.0.0.1:8000/indexKaryawan/1', 'Diterima', '2021-05-04', '', 3033940),
(44150222, 7818, '14', '12', 'berkas_karyawan/Tutorial_Instal_Predator_Sense_di_Acer_Nitro_5_by_RiF-Tekno.pdf', 'Diterima', '2021-05-04', 'y', 4191519),
(44150222, 7818, '10', '16', 'http://127.0.0.1:8000/indexKaryawan/Average%20Downtime/', 'Diterima', '2021-05-03', '', 7508241),
(44150222, 7818, '12', '13', 'https://www.google.com', 'Diterima', '2021-05-03', 'DIREVISI', 9115823);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(10) NOT NULL,
  `role` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `role`) VALUES
(1, 'Manager'),
(2, 'Karyawan');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `role_id` int(10) NOT NULL,
  `nama` text NOT NULL,
  `divisi` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `posisi` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `nama`, `divisi`, `email`, `username`, `password`, `foto`, `posisi`) VALUES
(3138303, 1, 'Test65', 'test 1', 'tester65@gmail.com', 'tester65', '123456', 'foto_user/photo_profile.png', 'test 2'),
(21682383, 2, 'Test 5', 'Project Manager', 'test5@gmail.com', 'test5', '123456', 'foto_user/photo_profile.png', 'Project Manager'),
(35054603, 1, 'Test 2', 'Engineer', 'test2@gmail.com', 'test21', '123456', 'foto_user/4eceb0346270022a22463c7539e74732_UaGF6UU.jpg', 'Software Engineer'),
(43556009, 1, 'Test 3', 'System Analyst', 'test3@gmail.com', 'test3', '123456', 'foto_user/photo_profile.png', 'System Analyst'),
(44150222, 2, 'Test 123', 'Engineer', 'test1@gmail.com', 'test13', 'Pretest123', 'foto_user/4eceb0346270022a22463c7539e74732_vRHvnPd_Eru8csb.jpg', 'Software Engineer'),
(51251100, 2, 'Tester', 'Engineer', 'tester@gmail.com', 'tester', '123456', 'foto_user/photo_profile.png', 'Software Engineer'),
(55997895, 1, 'Test 6', 'Project Manager', 'test6@gmail.com', 'test6', '123456', 'foto_user/photo_profile.png', 'Project Manager'),
(62302525, 2, 'Test 4', 'System Analyst', 'test4@gmail.com', 'test4', '123456', 'foto_user/photo_profile.png', 'System Analyst');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `divisi`
--
ALTER TABLE `divisi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `kpi`
--
ALTER TABLE `kpi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
