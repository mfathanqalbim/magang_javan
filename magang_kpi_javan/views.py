from django.shortcuts import render, redirect
from .models import User,Role,Review,Kpi,Divisi
import random, string
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User as UserDb
from .forms import UploadFoto
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Avg
# Create your views here.

def CreateUser(request):
    ids = ''.join(random.choices(string.digits, k=8))
    user_data = User.objects.all()
    divisi_data = Divisi.objects.all()
    errors = ""
    if request.method == "POST":
        if User.objects.filter(id=ids).exists():
            ids = ''.join(random.choices(string.digits, k=8))
        elif request.POST["email"] in User.objects.values_list('email', flat=True):
            errors = "Email sudah terdaftar, silahkan gunakan email lainnya."
        elif request.POST["username"] in User.objects.values_list('username', flat=True):
            errors = "Username sudah terdaftar, silahkan gunakan username lainnya."
        elif "admin" in str(request.POST["username"]):
            errors = "Username tidak boleh memiliki kata admin, silahkan gunakan username lainnya."
        else:
            picture = "foto_user/photo_profile.png"
            User.objects.create(
                    id = ids,
                    nama = request.POST['nama'].title(),
                    role = Role.objects.get(id=request.POST['role']),
                    email = request.POST['email'],
                    divisi = request.POST['divisi'],
                    username = request.POST['username'],
                    password = request.POST['password'],
                    foto = picture,
                    posisi= request.POST['posisi'],
                )
            users = UserDb.objects.create_user(request.POST['username'], request.POST['email'], request.POST['password'])
            return redirect('login')
    context = {
        "user_data":user_data,
        "divisi_data":divisi_data,
        "errors":errors,
    }
    return render(request, 'formRegister.html', context)

def LoginUser(request):
    errors = ""
    if request.method == "POST":
        if request.POST["username"] in User.objects.values_list('username', flat=True):
            if request.POST["password"] == User.objects.filter(username=request.POST["username"]).values_list("password", flat=True)[0]:
                users = authenticate(request, username=request.POST["username"], password=request.POST["password"])
                if users is not None:
                    login(request,users)
                    if User.objects.filter(username=request.POST["username"]).values_list("role", flat=True)[0] == 1:
                        return redirect("indexManager")
                    else:
                        data = User.objects.filter(username=request.user)
                        divisi_data = Divisi.objects.filter(divisi=data.values_list("divisi", flat=True)[0]).values_list("jenis_kpi",flat=True)
                        divisi_datas = divisi_data[0].split(",")
                        return redirect("http://127.0.0.1:8000/indexKaryawan/"+divisi_datas[0]+"/")
                else:
                    errors = "Username tidak ditemukan, silahkan ulangi lagi."
            else:
                errors = "Password yang anda masukkan salah, silahkan ulangi lagi."
        elif request.POST["username"] == "admin":
            if request.POST["password"] == "admin":
                users = authenticate(request, username="admin", password="admin")
                if users is not None:
                    login(request,users)
                    return redirect("adminuserpage")
        else:
            errors = "Username tidak ditemukan, silahkan ulangi lagi."
    context = {
        "errors": errors,
    }
    return render(request, 'formLogin.html', context)

def LoginAdmin(request):
    errors = ""
    if request.method == "POST":
        if request.POST["username"] == "admin":
            if request.POST["password"] == "admin":
                users = authenticate(request, username="admin", password="admin")
                if users is not None:
                    login(request,users)
                    return redirect("adminuserpage")
            else:
                errors = "Password salah, silahkan ulangi lagi."
        else:
            errors = "Username salah, silahkan ulangi lagi."
    context = {
        "errors": errors,
    }
    return render(request, 'adminLogin.html', context)

def IndexKaryawanDate(request, jenisKpi, selected_day):
    if str(request.user) in "AnonymousUser":
        return redirect('login')
    elif User.objects.filter(username=request.user).values_list("role", flat=True)[0] == 1:
        return redirect("indexManager")
    errors = ""
    data = User.objects.filter(username=request.user)
    divisi_data = Divisi.objects.filter(divisi=data.values_list("divisi", flat=True)[0]).values_list("jenis_kpi",flat=True)
    divisi_datas = divisi_data[0].split(",")
    divisi_datas = [a.lstrip() for a in divisi_datas]
    manager = User.objects.filter(role="1", divisi=data.values_list("divisi", flat=True)[0]).values_list("id", flat=True)[0]
    divisi_data1 = Kpi.objects.filter(user=str(manager))
    divisi_nilai = Kpi.objects.filter(user=str(manager), jenis_kpi=jenisKpi)
    tanggal = None
    if divisi_nilai.values_list("id",flat=True).exists():
        tanggal = datetime.strptime(selected_day, '%Y-%m-%d')
        tanggal = tanggal.replace(day=1)
        tanggal = Review.objects.filter(waktu__gte=tanggal.date(), waktu__lte=selected_day, user_id=data.values_list("id",flat=True)[0],kpi=divisi_nilai.values_list("id",flat=True)[0])
    uploadFoto = UploadFoto(request.POST, request.FILES)
    if request.method == "POST":
        if "editUs-" in str(request.POST.get("editUser")):
            getID = request.POST.get("editUser")
            if str(request.POST.get("email")) in User.objects.values_list('email', flat=True) and str(request.POST.get("email")) != data.values_list("email", flat=True)[0]:
                errors = "Email sudah terdaftar, silahkan gunakan email lainnya."
            elif str(request.POST.get("username")) in User.objects.values_list('username', flat=True) and str(request.POST.get("username")) != data.values_list("username", flat=True)[0]:
                errors = "Username sudah terdaftar, silahkan gunakan username lainnya."
            elif "admin" in str(request.POST.get("username")):
                errors = "Username tidak boleh memiliki kata admin, silahkan gunakan username lainnya."
            else:
                data1 = User.objects.get(id=getID.split("-")[1])
                User.objects.filter(id=getID.split("-")[1]).update(nama=str(request.POST.get("nama")), email=str(request.POST.get("email")), username=str(request.POST.get("pas"))
                ,password=str(request.POST.get("password")))
                if str(request.POST.get("gambar")) != "":
                    data1.foto = request.FILES["gambar"]
                data1.nama = str(request.POST.get("nama"))
                data1.email=str(request.POST.get("email"))
                data1.username=str(request.POST.get("username"))
                data1.password=str(request.POST.get("password"))
                data1.save()
                a = UserDb.objects.get(username=request.user)
                a.username = str(request.POST.get("username"))
                a.set_password(str(request.POST.get("password")))
                a.email = str(request.POST.get("email"))
                a.save()
        elif request.POST.get("logoutAdmin") == "logout-admin":
            logout(request)
            return redirect('login')
    context = {
        "users":data,
        "errors":errors,
        "uploadFoto": uploadFoto,
        "divisi_datas":divisi_datas,
        "divisi_data1":divisi_data1,
        "divisi_nilai":divisi_nilai,
        "pages":jenisKpi,
        "tanggal":tanggal,
    }
    return render(request, "indexKaryawanDate.html", context)

def IndexKaryawan(request, jenisKpi):
    if str(request.user) in "AnonymousUser":
        return redirect('login')
    elif User.objects.filter(username=request.user).values_list("role", flat=True)[0] == 1:
        return redirect("indexManager")
    errors = ""
    data = User.objects.filter(username=request.user)
    divisi_data = Divisi.objects.filter(divisi=data.values_list("divisi", flat=True)[0]).values_list("jenis_kpi",flat=True)
    divisi_datas = divisi_data[0].split(",")
    divisi_datas = [a.lstrip() for a in divisi_datas]
    manager = User.objects.filter(role="1", divisi=data.values_list("divisi", flat=True)[0]).values_list("id", flat=True)[0]
    divisi_data1 = Kpi.objects.filter(user=str(manager))
    divisi_nilai = Kpi.objects.filter(user=str(manager), jenis_kpi=jenisKpi)
    review = None
    tanggal = None
    not_allow = None
    total_score = None
    total_pencapaian = None
    if datetime.now().date().day < datetime.strptime("5",'%d').day:
        not_allow  = False
    else:
        not_allow  = True
    if divisi_nilai.values_list("id",flat=True).exists():
        total_score = Review.objects.filter(user_id=data.values_list("id",flat=True)[0],kpi=divisi_nilai.values_list("id",flat=True)[0]).values_list("score", flat=True).aggregate(Avg("score"))["score__avg"]
        total_pencapaian = Review.objects.filter(user_id=data.values_list("id",flat=True)[0],kpi=divisi_nilai.values_list("id",flat=True)[0]).values_list("pencapaian", flat=True).aggregate(Avg("pencapaian"))["pencapaian__avg"]
        tanggal = Review.objects.filter(user_id=data.values_list("id",flat=True)[0],kpi=divisi_nilai.values_list("id",flat=True)[0]).order_by("waktu")
        if Review.objects.filter(kpi=divisi_nilai.values_list("id",flat=True)[0], status="Revisi").exists():
            review = Review.objects.filter(user_id=data.values_list("id",flat=True)[0],kpi=divisi_nilai.values_list("id",flat=True)[0], status="Revisi")
        elif Review.objects.filter(kpi=divisi_nilai.values_list("id",flat=True)[0], status="Rejected").exists():
            review = Review.objects.filter(user_id=data.values_list("id",flat=True)[0],kpi=divisi_nilai.values_list("id",flat=True)[0], status="Rejected")
        elif Review.objects.filter(kpi=divisi_nilai.values_list("id",flat=True)[0], status="Pending").exists():
            review = Review.objects.filter(user_id=data.values_list("id",flat=True)[0],kpi=divisi_nilai.values_list("id",flat=True)[0], status="Pending")
        else:
            review = sorted(Review.objects.filter(user_id=data.values_list("id",flat=True)[0],kpi=divisi_nilai.values_list("id",flat=True)[0]), key = lambda x: x.waktu, reverse=True)
    role_list = Role.objects.all()
    uploadFoto = UploadFoto(request.POST, request.FILES)
    if request.method == "POST":
        if "editUs-" in str(request.POST.get("editUser")):
            getID = request.POST.get("editUser")
            if str(request.POST.get("email")) in User.objects.values_list('email', flat=True) and str(request.POST.get("email")) != data.values_list("email", flat=True)[0]:
                errors = "Email sudah terdaftar, silahkan gunakan email lainnya."
            elif str(request.POST.get("username")) in User.objects.values_list('username', flat=True) and str(request.POST.get("username")) != data.values_list("username", flat=True)[0]:
                errors = "Username sudah terdaftar, silahkan gunakan username lainnya."
            elif "admin" in str(request.POST.get("username")):
                errors = "Username tidak boleh memiliki kata admin, silahkan gunakan username lainnya."
            else:
                data1 = User.objects.get(id=getID.split("-")[1])
                User.objects.filter(id=getID.split("-")[1]).update(nama=str(request.POST.get("nama")), email=str(request.POST.get("email")), username=str(request.POST.get("pas"))
                ,password=str(request.POST.get("password")))
                if str(request.POST.get("gambar")) != "":
                    data1.foto = request.FILES["gambar"]
                data1.nama = str(request.POST.get("nama"))
                data1.email=str(request.POST.get("email"))
                data1.username=str(request.POST.get("username"))
                data1.password=str(request.POST.get("password"))
                data1.save()
                a = UserDb.objects.get(username=request.user)
                a.username = str(request.POST.get("username"))
                a.set_password(str(request.POST.get("password")))
                a.email = str(request.POST.get("email"))
                a.save()
        elif "editKpi-" in str(request.POST.get("editKPI")):
            if int(request.POST["kpiScore"]) > int(divisi_nilai.values_list("bobot", flat=True)[0]):
                errors = "Nilai KPI Score tidak boleh lebih dari nilai Bobot."
            else:
                getID = request.POST.get("editKPI")
                if Review.objects.filter(id=getID.split("-")[1]).values_list("status", flat=True)[0] == "Rejected":
                    Review.objects.filter(id=getID.split("-")[1]).update(pencapaian=str(request.POST["pencapaian"]), score=str(request.POST["kpiScore"]), status="Revisi")
                    review = Review.objects.filter(user_id=data.values_list("id",flat=True)[0],kpi=divisi_nilai.values_list("id",flat=True)[0], status="Revisi")
                    datas = Review.objects.get(id=getID.split("-")[1])
                    datas.berkas = request.FILES.get("linkFile")
                    datas.save()
                else:
                    Review.objects.filter(id=getID.split("-")[1]).update(pencapaian=str(request.POST["pencapaian"]), score=str(request.POST["kpiScore"]))
                    review = sorted(Review.objects.filter(kpi=divisi_nilai.values_list("id",flat=True)[0]), key = lambda x: x.waktu, reverse=True)
                    datas = Review.objects.get(id=getID.split("-")[1])
                    datas.berkas = request.FILES.get("linkFile")
                    datas.save()
        elif request.POST.get("tambahKpi") == "tambahKPI":
            if int(request.POST["kpiScore"]) > int(divisi_nilai.values_list("bobot", flat=True)[0]):
                errors = "Nilai KPI Score tidak boleh lebih dari nilai Bobot."
            else:
                ids = ''.join(random.choices(string.digits, k=7))
                if Review.objects.filter(id=ids).exists():
                    ids = ''.join(random.choices(string.digits, k=7))
                Review.objects.filter(kpi=divisi_nilai.values_list("id",flat=True)[0]).order_by("waktu").create(id=ids, user_id=data.values_list("id", flat=True)[0], waktu=datetime.now(),
                kpi=Kpi.objects.get(id=divisi_nilai.values_list("id",flat=True)[0]),pencapaian=str(request.POST["pencapaian"]), 
                score=str(request.POST["kpiScore"]),berkas=request.FILES.get("linkFile"), status="Pending", note="")
                datas = Review.objects.get(id=ids)
                datas.berkas = request.FILES.get("linkFile")
                datas.save()
        elif request.POST.get("logoutAdmin") == "logout-admin":
            logout(request)
            return redirect('login')
    context = {
        "users":data,
        "role_list":role_list,
        "errors":errors,
        "uploadFoto": uploadFoto,
        "divisi_datas":divisi_datas,
        "divisi_data1":divisi_data1,
        "divisi_nilai":divisi_nilai,
        "review":review,
        "pages":jenisKpi,
        "tanggal":tanggal,
        "allowed": not_allow,
        "total_score":total_score,
        "total_pencapaian":total_pencapaian,
    }
    return render(request, 'indexKaryawan.html', context)

def IndexManager(request):
    if str(request.user) in "AnonymousUser":
        return redirect('login')
    elif User.objects.filter(username=request.user).values_list("role", flat=True)[0] == 2:
        return redirect("indexKaryawan")
    user_list = User.objects.order_by("nama").filter(divisi=User.objects.filter(username=request.user).values_list("divisi", flat=True)[0]).exclude(username=request.user, role="1")
    if request.method == "GET":
        if "cari" in request.GET:   
            if str(request.GET["cari"]) != "":
                nama_ = request.GET["cari"]
                user_list = User.objects.filter(nama=nama_, divisi=User.objects.filter(username=request.user).values_list("divisi", flat=True)[0]).exclude(username=request.user, role="1")
            else:
                user_list = User.objects.order_by("nama").filter(divisi=User.objects.filter(username=request.user).values_list("divisi", flat=True)[0]).exclude(username=request.user, role="1")
        else:
            user_list = User.objects.order_by("nama").filter(divisi=User.objects.filter(username=request.user).values_list("divisi", flat=True)[0]).exclude(username=request.user, role="1")
    errors = ""
    data = User.objects.filter(username=request.user)
    role_list = Role.objects.all()
    divisi_data = Divisi.objects.filter(divisi=data.values_list("divisi", flat=True)[0]).values_list("jenis_kpi",flat=True)
    divisi_datas = divisi_data[0].split(",")
    divisi_datas = [a.lstrip() for a in divisi_datas]
    uploadFoto = UploadFoto(request.POST, request.FILES)
    if request.method == "POST":
        if "editUs-" in str(request.POST.get("editUser")):
            getID = request.POST.get("editUser")
            if str(request.POST.get("email")) in User.objects.values_list('email', flat=True) and str(request.POST.get("email")) != data.values_list("email", flat=True)[0]:
                errors = "Email sudah terdaftar, silahkan gunakan email lainnya."
            elif str(request.POST.get("username")) in User.objects.values_list('username', flat=True) and str(request.POST.get("username")) != data.values_list("username", flat=True)[0]:
                errors = "Username sudah terdaftar, silahkan gunakan username lainnya."
            elif "admin" in str(request.POST.get("username")):
                errors = "Username tidak boleh memiliki kata admin, silahkan gunakan username lainnya."
            else:
                data.update(nama=request.POST["nama"], email=request.POST["email"], username=request.POST["username"],password=request.POST["password"])
                UserDb.objects.update(username=request.POST["username"],password=request.POST["password"])
                data1 = User.objects.get(id=getID.split("-")[1])
                if str(request.POST.get("gambar")) != "":
                    data1.foto = request.FILES["gambar"]
                data1.save()
                data = User.objects.filter(username=request.user)
        elif request.POST.get("updateKpi") == "updateScore":
            ids = ''.join(random.choices(string.digits, k=8))
            if Kpi.objects.filter(id=ids).exists():
                ids = ''.join(random.choices(string.digits, k=8))
            Kpi.objects.create(
                id=ids,
                user=User.objects.get(id=data.values_list("id", flat=True)[0]),
            )
        elif request.POST.get("logoutAdmin") == "logout-admin":
            logout(request)
            return redirect('login')
    page = request.GET.get('page')
    paginator = Paginator(user_list, 5)
    page_obj = paginator.get_page(page)
    context = {
        "users": page_obj,
        "user_list":user_list,
        "users1":data,
        "role_list":role_list,
        "errors":errors,
        "uploadFoto": uploadFoto,
        "divisi_datas":divisi_datas,
    }
    return render(request, 'indexManager.html', context)

def KpiManager(request, jenisKpi, id):
    if User.objects.filter(id=str(id)).exclude(username=request.user).exists():
        page = request.GET.get('page')
        data = User.objects.filter(username=request.user)
        if str(data[0]) not in str(Kpi.objects.filter(jenis_kpi=str(jenisKpi), user=data[0]).values_list("user", flat=True)):
            ids = ''.join(random.choices(string.digits, k=4))
            if Kpi.objects.filter(user=data[0]).exists():
                ids = ''.join(random.choices(string.digits, k=4))
            Kpi.objects.create(
                id=ids,
                user=User.objects.get(id=data.values_list("id", flat=True)[0]),
                jenis_kpi=str(jenisKpi),
                bobot=str(10), 
                target=str(15)
            )
        divisi_data = Divisi.objects.filter(divisi=data.values_list("divisi", flat=True)[0]).values_list("jenis_kpi",flat=True)
        divisi_datas = divisi_data[0].split(",")
        divisi_datas = [a.lstrip() for a in divisi_datas]
        kpi_ids = Kpi.objects.get(jenis_kpi=str(jenisKpi), user=data[0])
        user_list = Review.objects.filter(user_id=str(id), kpi=kpi_ids)
        data_user = Review.objects.filter(user_id=str(id))
        paginator = Paginator(user_list, 5)
        page_obj = paginator.get_page(page)
        errors = ""
        uploadFoto = UploadFoto(request.POST, request.FILES)
        if request.method == "POST":
            if "tolakBerkas-" in str(request.POST.get("tolakKpi")):
                getID = request.POST.get("tolakKpi")
                data_review = Review.objects.get(user_id=getID.split("-")[1], kpi=str(kpi_ids), id=getID.split("-")[2])
                Review.objects.filter(id=str(data_review)).update(status="Rejected", note=str(request.POST.get("catatan")))
            elif "updatePending-" in str(request.POST.get("pendingKpi")):
                getID = request.POST.get("pendingKpi")
                data_review = Review.objects.get(user_id=getID.split("-")[1], kpi=str(kpi_ids), id=getID.split("-")[2])
                Review.objects.filter(id=str(data_review)).update(status="On Review")
            elif "updateReview-" in str(request.POST.get("reviewKpi")):
                getID = request.POST.get("reviewKpi")
                data_review = Review.objects.get(user_id=getID.split("-")[1], kpi=str(kpi_ids), id=getID.split("-")[2])
                Review.objects.filter(id=str(data_review)).update(status="Diterima", note=str(request.POST.get("catatan")))
            elif "editUs-" in str(request.POST.get("editUser")):
                getID = request.POST.get("editUser")
                if str(request.POST.get("email")) in User.objects.values_list('email', flat=True) and str(request.POST.get("email")) != data.values_list("email", flat=True)[0]:
                    errors = "Email sudah terdaftar, silahkan gunakan email lainnya."
                elif str(request.POST.get("username")) in User.objects.values_list('username', flat=True) and str(request.POST.get("username")) != data.values_list("username", flat=True)[0]:
                    errors = "Username sudah terdaftar, silahkan gunakan username lainnya."
                elif "admin" in str(request.POST.get("username")):
                    errors = "Username tidak boleh memiliki kata admin, silahkan gunakan username lainnya."
                else:
                    data.update(email=str(request.POST.get("email")), nama=str(request.POST.get("nama")), password=str(request.POST.get("password")))
                    data1 = User.objects.get(id=getID.split("-")[1])
                    if str(request.POST.get("gambar")) != "":
                        data1.foto = request.FILES["gambar"]
                    data1.save()
            elif request.POST.get("updateKpi") == "updateScore":
                Kpi.objects.filter(jenis_kpi=str(jenisKpi), user=data[0]).update(bobot=str(request.POST.get("bobot")), target=str(request.POST.get("target")))
                kpi_ids = Kpi.objects.get(jenis_kpi=str(jenisKpi), user=data[0])
            elif request.POST.get("logoutAdmin") == "logout-admin":
                logout(request)
                return redirect('login')
        context = {
            "users": page_obj,
            "users1":data,
            "kpi_ids":kpi_ids,
            "user_list":user_list,
            "data_user":data_user,
            "errors":errors,
            "uploadFoto": uploadFoto,
            "divisi_datas":divisi_datas,
            "iduser":id,
        }
        return render(request, 'indexManagerKpi.html',context)
    else:
        return redirect("indexManager")


def AdminPageUser(request):
    if str(request.user) in "AnonymousUser":
        return redirect('login')
    elif str(request.user) not in "admin":
        return redirect('login')
    errors = ""
    if request.method == "GET":
        if "cari" in request.GET:
            if str(request.GET["cari"]) != "":
                nama_ = request.GET["cari"]
                user_list = User.objects.filter(nama=nama_)
            else:
                user_list = User.objects.order_by("nama")
        else:
            user_list = User.objects.order_by("nama")
    elif request.method == "POST":
        if "hapusModal-" in str(request.POST.get("delUser")):
            getID = request.POST.get("delUser")
            data = User.objects.get(id=getID.split("-")[1])
            data.delete()
            UserDb.objects.filter(username=data.username).delete()
            user_list = User.objects.order_by("nama")
        elif request.POST.get("logoutAdmin") == "logout-admin":
            logout(request)
            return redirect("login")
        elif request.POST.get("tambahUser") == "addUser":
            ids = ''.join(random.choices(string.digits, k=8))
            if User.objects.filter(id=ids).exists():
                ids = ''.join(random.choices(string.digits, k=8))
            elif request.POST["email"] in User.objects.values_list('email', flat=True):
                errors = "Email sudah terdaftar, silahkan gunakan email lainnya."
            elif request.POST["username"] in User.objects.values_list('username', flat=True):
                errors = "Username sudah terdaftar, silahkan gunakan username lainnya."
            elif "admin" in str(request.POST["username"]):
                errors = "Username tidak boleh memiliki kata admin, silahkan gunakan username lainnya."
            else:
                picture = "foto_user/photo_profile.png"
                User.objects.create(
                        id = ids,
                        nama = request.POST['nama'].title(),
                        role = Role.objects.get(id=request.POST['role']),
                        email = request.POST['email'],
                        divisi = request.POST['divisi'],
                        username = request.POST['username'],
                        password = request.POST['password'],
                        foto = picture,
                        posisi= request.POST['posisi'],
                    )
                users = UserDb.objects.create_user(request.POST['username'], request.POST['email'], request.POST['password'])
            user_list = User.objects.order_by("nama")
        elif "editUs-" in str(request.POST.get("editUser")):
            getID = request.POST.get("editUser")
            data = User.objects.get(id=getID.split("-")[1])
            datas = User.objects.filter(id=str(data))
            if request.POST["email"] in User.objects.values_list('email', flat=True) and str(request.POST.get("email")) != datas.values_list("email", flat=True)[0]:
                errors = "Email sudah terdaftar, silahkan gunakan email lainnya."
            elif request.POST["username"] in User.objects.values_list('username', flat=True) and str(request.POST.get("username")) != datas.values_list("username", flat=True)[0]:
                errors = "Username sudah terdaftar, silahkan gunakan username lainnya."
            elif "admin" in str(request.POST["username"]):
                errors = "Username tidak boleh memiliki kata admin, silahkan gunakan username lainnya."
            else:
                if str(request.POST.get("gambar")) != "":
                    data.foto = request.FILES["gambar"]
                data.save()
                UserDb.objects.update(username=request.POST["username"],password=request.POST["password"])
                datas.update(
                        nama = request.POST['nama'].title(),
                        role = Role.objects.get(id=request.POST['role']),
                        email = request.POST['email'],
                        divisi = str(request.POST.get('divisi1')),
                        username = request.POST['username'],
                        password = request.POST['password'],
                        posisi= str(request.POST.get('posisi1')),
                    )
            user_list = User.objects.order_by("nama")
    role_list = Role.objects.all()
    page = request.GET.get('page')
    paginator = Paginator(user_list, 4)
    page_obj = paginator.get_page(page)
    context = { 'users': page_obj, "user_list":user_list, "role_list":role_list, "errors":errors,}
    return render(request, 'adminUserPage.html', context)

def AdminPageDivisi(request):
    if str(request.user) in "AnonymousUser":
        return redirect('login')
    elif str(request.user) not in "admin":
        return redirect('login')
    errors = ""
    if request.method == "GET":
        if "cari" in request.GET:
            if str(request.GET["cari"]) != "":
                nama_ = request.GET["cari"]
                user_list = Divisi.objects.filter(divisi=nama_)
            else:
                user_list = Divisi.objects.order_by("divisi")
        else:
            user_list = Divisi.objects.order_by("divisi")
    elif request.method == "POST":
        if "hapusModal-" in str(request.POST.get("delDivisi")):
            getID = request.POST.get("delDivisi")
            data = Divisi.objects.get(id=getID.split("-")[1])
            data.delete()
            user_list = Divisi.objects.order_by("divisi")
        elif request.POST.get("logoutAdmin") == "logout-admin":
            logout(request)
            return redirect("login")
        elif request.POST.get("tambahDivisi") == "addDivisi":
            ids = ''.join(random.choices(string.digits, k=8))
            if User.objects.filter(id=ids).exists():
                ids = ''.join(random.choices(string.digits, k=8))
            elif request.POST["divisi"] in Divisi.objects.values_list('divisi', flat=True):
                errors = "Divisi sudah terdaftar, silahkan gunakan divisi lainnya."
            elif request.POST["posisi"] in Divisi.objects.values_list('posisi', flat=True):
                errors = "Posisi sudah terdaftar, silahkan gunakan posisi lainnya."
            else:
                Divisi.objects.create(
                        id = ids,
                        divisi = request.POST['divisi'],
                        posisi = request.POST['posisi'],
                        jenis_kpi = request.POST['jenisKpi'],
                    )
            user_list = Divisi.objects.order_by("divisi")
        elif "editDiv-" in str(request.POST.get("editDivisi")):
            getID = request.POST.get("editDivisi")
            data = Divisi.objects.get(id=getID.split("-")[1])
            datas = Divisi.objects.filter(id=str(data))
            if request.POST["divisi"] in Divisi.objects.values_list('divisi', flat=True) and str(request.POST.get("divisi")) != datas.values_list("divisi", flat=True)[0]:
                errors = "Divisi sudah terdaftar, silahkan gunakan divisi lainnya."
            elif request.POST["posisi"] in Divisi.objects.values_list('posisi', flat=True) and str(request.POST.get("posisi")) != datas.values_list("posisi", flat=True)[0]:
                errors = "Posisi sudah terdaftar, silahkan gunakan Posisi lainnya."
            elif len(request.POST['jenisKpi'].split(",")) > 7 and str(request.POST["divisi"]) != "Engineer":
                errors = "Jenis KPI tidak boleh melebihi 6."
            elif len(request.POST['jenisKpi'].split(",")) > 6 and str(request.POST["divisi"]) == "Engineer":
                errors = "Jenis KPI tidak boleh melebihi 5."
            else:
                datas.update(
                        divisi = request.POST['divisi'],
                        posisi = request.POST["posisi"],
                        jenis_kpi = request.POST['jenisKpi'],
                    )
            user_list = Divisi.objects.order_by("divisi")
    page = request.GET.get('page')
    paginator = Paginator(user_list, 4)
    page_obj = paginator.get_page(page)
    context = { 'users': page_obj, "user_list":user_list, "errors":errors,}
    return render(request, 'adminDivisiPage.html', context)