from django.contrib import admin
from .models import Role, User, Kpi, Review, Divisi
# Register your models here.

admin.site.register(Role)
admin.site.register(User)
admin.site.register(Review)
admin.site.register(Kpi)
admin.site.register(Divisi)
