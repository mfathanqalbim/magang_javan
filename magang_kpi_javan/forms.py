from django import forms
from datetime import datetime
class UploadFoto(forms.Form):
    gambar = forms.CharField(label="gambar", widget=forms.FileInput(attrs={"type":"file", "id":"file-input", "accept":".png, .jpg, .jpeg", "class":"form-control"}), required=False)
    date = forms.DateTimeField(input_formats=['%m/%Y'], widget=forms.DateTimeInput(attrs={"type":"month", "value":datetime.today().strftime("%Y-%m"), "id":"dates",
    "min":"2013-01", "size":"8", "name":"dates", "autocomplete":"off"}))